# Teste prático - Agência Inbound

## Objetivos
Desenvolvimento de tema para hubspot 

## Observações
1 - Deve se manter a estrutura padrão de temas do projeto, podendo adicionar os assets que forem necessários

2 - Todos as seções e módulos devem ter seu conteúdo padrão configurados no template.

3 - Header e footer devem estar em uma partial global

4 - O componente de "Palestrantes" deve ser um módulo customizado utilizando um repeater para o cadastro dos palestrantes

5 - Utilização de Theme Fields é opcional

6 - Todo o trabalho deve ser feito em sua conta de desenvolvedor da Hubspot (item 5 da documentação)

7 - Todo o conteúdo do templates, incluindo configurações da seção, devem ser editáveis através do editor de páginas

8 - O template deve ser pixel perfect, respeitando todos os estilos (fontes, espaçamentos e etc)

## Documentações
1 - https://developers.hubspot.com/docs/cms/building-blocks/themes

2 - https://developers.hubspot.com/docs/cms/guides/getting-started-with-local-development

3 - https://developers.hubspot.com/docs/cms/guides/creating-a-drag-and-drop-area

4 - https://developers.hubspot.com/docs/cms/guides/getting-started-with-modules

5 - https://developers.hubspot.com/get-started

## Recursos
Figma: https://www.figma.com/file/yIn8pqmkl8CHNk5ziEo76d/Hubspot-Test?node-id=802%3A2&t=EBe1TW5NI9sTOxqB-1




